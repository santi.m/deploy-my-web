FROM bitnami/nginx:1.16

WORKDIR /app

COPY /web .

EXPOSE 8080

ENTRYPOINT [ "/entrypoint.sh" ]
CMD [ "/run.sh" ]