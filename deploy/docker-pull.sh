#!/bin/bash

set -ex

REPO="registry.gitlab.com/santi.m/deploy-my-web"
GITLAB_USERNAME=""
GITLAB_PASSWORD=""

sudo docker login registry.gitlab.com -u $GITLAB_USERNAME -p $GITLAB_PASSWORD
sudo docker pull $REPO:latest
sudo docker-compose down
sudo docker-compose up -d
